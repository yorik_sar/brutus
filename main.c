#define _GNU_SOURCE

#include <stdio.h>
#include <string.h>
#include <crypt.h>
#include <unistd.h>

#define MAX_LEN 10

char dict[] = "abcdefghijklmnopqrstuvwxyz";

#define DICT_SIZE (sizeof(dict) - 1)

typedef struct {
    char target_hash[13];
    char guess[MAX_LEN + 1];
    int prefix_len;
    struct crypt_data crypt_ctx;
} bt_ctx;

void
bt_ctx_init(bt_ctx *ctx, const char target_hash[13]) {
    memcpy(ctx->target_hash, target_hash, 13);
    memset(ctx->guess, 0, MAX_LEN + 1);
    ctx->prefix_len = 0;
    ctx->crypt_ctx.initialized = 0;
}

int
brute(bt_ctx *ctx) {
    unsigned char indices[MAX_LEN];
    int i;
    char *guessed_hash;

    memset(indices, 0, MAX_LEN);
    for (;;) {
        for (i = 0; (i < MAX_LEN) && (indices[i] == DICT_SIZE + 1); i++) {
            indices[i] = 1;
            ctx->guess[i] = dict[0];
        }
        if (i == MAX_LEN) {
            break;
        }
        ctx->guess[i] = dict[indices[i]++];
        guessed_hash = crypt_r(ctx->guess, ctx->target_hash, &ctx->crypt_ctx);
        if (!memcmp(guessed_hash, ctx->target_hash, 13)) {
            return 1;
        }
    }
    return 0;
}

int
main(int argc, char **argv) {
    bt_ctx ctx;

    if (argc != 2 || strlen(argv[1]) != 13) {
        return 1;
    }
    bt_ctx_init(&ctx, argv[1]);
    if (brute(&ctx)) {
        write(1, ctx.guess, strlen(ctx.guess));
        return 0;
    } else {
        return 2;
    }
}
