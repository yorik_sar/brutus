CC=clang
CFLAGS=-Wall -Wextra -Werror
LDFLAGS=-lcrypt -lrt
SOURCES=main.c
OBJECTS=$(SOURCES:.c=.o)
EXECUTABLE=main

all: $(SOURCES) $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS)
