#include <stdio.h>
#include <stdlib.h>
#include <crypt.h>
#include <unistd.h>

const char salt_dict[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789./";

int
main(int argc, char **argv) {
    char salt[2];
    long rnd;

    if (argc != 2) {
        return 1;
    }
    rnd = random();
    salt[0] = salt_dict[rnd & 63];
    salt[1] = salt_dict[(rnd >> 6) & 63];
    write(1, crypt(argv[1], salt), 13);
    return 0;
}
